/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/**
 * @author Teodor MAMOLEA
 */
public class PaintPanel extends JPanel implements MouseListener, MouseMotionListener, ActionListener {
    static private final int NULL = 0;
    static private final int MY_POINT = (2 << 30);
    static private final int PARTNER_POINT = (2 << 29);
    static private final int MY_PRISONIER_POINT = (2 << 28);
    static private final int PARTNER_PRISONIER_POINT = (2 << 27);
    static private final int MARK_POINT = (2 << 26);
    static private final int NULL_POINT = ~(MY_POINT | PARTNER_POINT | MARK_POINT);
    static private final int FIRST_MARK_ID = 1;
    private final SecondFrame parent;
    private final int _HEIGHT;
    private final int _WIDTH;
    private final Rectangle mobilRect = new Rectangle();
    private final Rectangle staticRect = new Rectangle();
    private final int CELL_H = 20;
    private final int CELL_W = CELL_H;
    private final int LINES = 50;
    private final int COLUMNS = 50;
    private final int PADDING = 10;
    private final int RASE = 6;
    private final int DIAMETER = 2 * RASE;
    private final int PADDING_MINUS_RASE = PADDING - RASE;
    private final int PANEL_W = CELL_W * COLUMNS + 2 * PADDING;
    private final int PANEL_H = CELL_H * LINES + 2 * PADDING;
    private final int STATICRECT_W = 170;
    private final int STATICRECT_H = 170;
    private final long SCALE_X = PANEL_W / STATICRECT_W;
    private final long SCALE_Y = PANEL_H / STATICRECT_H;
    private final BufferedImage image = new BufferedImage(PANEL_W, PANEL_H, BufferedImage.TYPE_INT_RGB);
    private final java.awt.Graphics2D g2Image = image.createGraphics();
    private final Color drawRectColor = Color.BLACK;
    private final Color fillRectColor = new Color(0, 0, 0, 100);
    private final Color lineColor = Color.LIGHT_GRAY;
    private final Color backgroundColor = new Color(0xee, 0xee, 0xee);
    private final Color myColor = Color.BLUE;
    private final Color myTransparentColor = new Color(0, 0, 255, 100);
    private final Color partnerColor = Color.RED;
    private final Color partnerTransparentColor = new Color(255, 0, 0, 100);
    private final Color okColor = new Color(0, 255, 0, 150);
    private final Color nokColor = new Color(255, 0, 0, 150);
    private final int[][] pointsTable = new int[COLUMNS + 1][LINES + 1];
    private final Point lastPointedPoint = new Point(0, 0);


    /*

     | | |
    -1-2-3-
     | | |
    -8-O-4-
     | | |
    -7-6-5-
     | | |

    O - current point
    */
    private final LocalJMenuItem setTopLeftAlignment;
    private final LocalJMenuItem setTopRightAlignment;
    private final LocalJMenuItem setBottomLeftAlignment;
    private final LocalJMenuItem setBottomRightAlignment;
    private final Polygon pixelsPolygon = new Polygon();
    private final Polygon pointsPolygon = new Polygon();
    public boolean myTurn;
    private boolean pointInStaticRect;
    private boolean pointInMobilRect;
    private Point lastMovedPoint;
    private int dxFromMobilRectLocation;
    private int dyFromMobilRectLocation;
    private int STATICRECT_X = 10;
    private int STATICRECT_Y;
    private int locationX;
    private int locationY;
    private boolean lastPointedPointOK;
    private Point lastMyPutPoint;
    private Point lastPartnerPutPoint;
    private boolean aroundPoint;

    public PaintPanel(final SecondFrame parent, final int _WIDTH, final int _HEIGHT) {
        super(null);

        this.parent = parent;
        this._HEIGHT = _HEIGHT;
        this._WIDTH = _WIDTH;
        STATICRECT_Y = _HEIGHT - STATICRECT_X - STATICRECT_H;

        setPreferredSize(new Dimension(PANEL_W, PANEL_H + 2 + DIAMETER));
        setSize(getPreferredSize());

        JPopupMenu popup = new JPopupMenu();

        setTopLeftAlignment = new LocalJMenuItem("Top Left", "1", this, popup);
        setTopRightAlignment = new LocalJMenuItem("Top Right", "2", this, popup);
        setBottomLeftAlignment = new LocalJMenuItem("Bottom Left", "3", this, popup);
        setBottomRightAlignment = new LocalJMenuItem("Bottom Right", "4", this, popup);

        setComponentPopupMenu(popup);

        init();
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void clear() {
        init();
    }

    private void init() {
        setLocation(locationX = 0, locationY = 0);

        staticRect.setSize(STATICRECT_W, STATICRECT_H);
        mobilRect.setBounds(0, 0, (_WIDTH * STATICRECT_W) / PANEL_W, (_HEIGHT * STATICRECT_H) / PANEL_H);
        setStaticRectAlign("3");

        lastMyPutPoint = null;
        lastPartnerPutPoint = null;
        lastPointedPointOK = false;
        aroundPoint = false;
        for (int i = 0, j; i <= COLUMNS; ++i) {
            for (j = 0; j <= LINES; ++j) {
                pointsTable[i][j] = NULL;
            }
        }

        g2Image.setPaint(backgroundColor);
        g2Image.fill(new Rectangle(0, 0, PANEL_W, PANEL_H));

        g2Image.setPaint(Color.LIGHT_GRAY);

        for (int i = 0; i <= COLUMNS; ++i) {
            g2Image.drawLine(i * CELL_W + PADDING, 0, i * CELL_W + PADDING, PANEL_H);
        }

        for (int i = 0; i <= LINES; ++i) {
            g2Image.drawLine(0, i * CELL_H + PADDING, PANEL_W, i * CELL_H + PADDING);
        }
    }

    public void input(final String input) {
        int spacePos = input.indexOf(' ');

        int x = Integer.parseInt(input.substring(1, spacePos));
        int y = Integer.parseInt(input.substring(spacePos + 1));

        setPartnerPoint(x, y);

        pixelsPolygon.reset();
        pointsPolygon.reset();
        findNextPoint(x, y, FIRST_MARK_ID, PARTNER_POINT);
        updateStatistic(PARTNER_PRISONIER_POINT);

        x = x * CELL_W + PADDING_MINUS_RASE;
        y = y * CELL_H + PADDING_MINUS_RASE;

        lastPartnerPutPoint = new Point((int) (x / SCALE_X), (int) (y / SCALE_Y));

        fillPoint(x, y, partnerColor, partnerTransparentColor);

        myTurn = true;
    }

    public void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, null);
        repaintRects((Graphics2D) g);
        paintCurrentPointedPointIntersection(g);
    }

    private void repaintRects(Graphics2D g2) {
        paintStaticRect(g2);
        paintMobilRect(g2);

        int x;
        int y;

        try {
            x = lastMyPutPoint.x + staticRect.x;
            y = lastMyPutPoint.y + staticRect.y;

            g2.setColor(myColor);
            g2.drawOval(x - 4, y - 4, 10, 10);
        } catch (NullPointerException ex) {
        }

        try {
            x = lastPartnerPutPoint.x + staticRect.x;
            y = lastPartnerPutPoint.y + staticRect.y;

            g2.setColor(partnerColor);
            g2.drawOval(x - 4, y - 4, 10, 10);
        } catch (NullPointerException ex) {
        }
    }

    private void paintStaticRect(Graphics2D g2) {
        g2.drawImage(image.getScaledInstance(STATICRECT_W, STATICRECT_H, Image.SCALE_SMOOTH),
                staticRect.x,
                staticRect.y,
                null
        );
        g2.setPaint(drawRectColor);
        g2.drawRect(staticRect.x - 1, staticRect.y - 1, staticRect.width + 1, staticRect.height + 1);
    }

    private void paintMobilRect(Graphics2D g2) {
        g2.setPaint(fillRectColor);
        g2.fill(mobilRect);
    }

    private void updateMouseStates(MouseEvent e) {
        lastMovedPoint = e.getPoint();
        if (mobilRect.contains(lastMovedPoint)) {
            pointInMobilRect = true;
            pointInStaticRect = true;
            dxFromMobilRectLocation = lastMovedPoint.x - mobilRect.x;
            dyFromMobilRectLocation = lastMovedPoint.y - mobilRect.y;
            repaintLastPointedPointIntersection(getGraphics());
        } else {
            pointInMobilRect = false;
            if (pointInStaticRect = staticRect.contains(lastMovedPoint)) {
                repaintLastPointedPointIntersection(getGraphics());
            }
        }
    }

    private void relocation(Point cursorPoint, int dx, int dy) {

        mobilRect.setLocation(cursorPoint);

        mobilRect.x -= dx;
        mobilRect.y -= dy;
        if (mobilRect.x < staticRect.x)
            mobilRect.x = staticRect.x;
        else if (mobilRect.x + mobilRect.width > staticRect.x + staticRect.width)
            mobilRect.x = staticRect.x + staticRect.width - mobilRect.width;

        if (mobilRect.y < staticRect.y)
            mobilRect.y = staticRect.y;
        else if (mobilRect.y + mobilRect.height > staticRect.y + staticRect.height)
            mobilRect.y = staticRect.y + staticRect.height - mobilRect.height;

        locationX = -(int) ((mobilRect.x - staticRect.x) * SCALE_X + 0.5);
        locationY = -(int) ((mobilRect.y - staticRect.y) * SCALE_Y + 0.5);

        dx = staticRect.x;
        dy = staticRect.y;

        staticRect.setLocation(-locationX + STATICRECT_X, -locationY + STATICRECT_Y);

        dx = staticRect.x - dx;
        dy = staticRect.y - dy;

        mobilRect.setLocation(mobilRect.x + dx, mobilRect.y + dy);
        setLocation(locationX, locationY);
    }

    private void fillPoint(int x, int y, final Color color, final Color polygonColor) {
        g2Image.setPaint(color);
        g2Image.fillOval(x, y, DIAMETER, DIAMETER);
        g2Image.draw(pixelsPolygon);
        g2Image.setPaint(polygonColor);
        g2Image.fill(pixelsPolygon);

        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (pointInStaticRect) {
            updateMouseStates(e);
            relocation(e.getPoint(), mobilRect.width / 2, mobilRect.height / 2);
        } else {
            if (lastPointedPointOK && myTurn) {
                myTurn = false;
                setMyPoint(lastPointedPoint.x, lastPointedPoint.y);
                parent.send("" + Constants.POINT + lastPointedPoint.x + " " + lastPointedPoint.y);

                int x = lastPointedPoint.x * CELL_W + PADDING_MINUS_RASE;
                int y = lastPointedPoint.y * CELL_H + PADDING_MINUS_RASE;

                lastMyPutPoint = new Point((int) (x / SCALE_X), (int) (y / SCALE_Y));

                System.out.println();

                pixelsPolygon.reset();
                pointsPolygon.reset();
                findNextPoint(lastPointedPoint.x, lastPointedPoint.y, FIRST_MARK_ID, MY_POINT);
                updateStatistic(MY_PRISONIER_POINT);

                fillPoint(x, y, myColor, myTransparentColor);
                lastPointedPointOK = false;
            }
        }
//        print( g2Image );
//        try {
//            ImageIO.write( image, "jpg", File("hahaha" + Math.random()));
//        } catch (IOException ex) {
//
//        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {
        repaintLastPointedPointIntersection(getGraphics());
        aroundPoint = false;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (pointInMobilRect)
            relocation(e.getPoint(), dxFromMobilRectLocation, dyFromMobilRectLocation);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        updateMouseStates(e);
        if (pointInStaticRect) return;

        int x = e.getX() - PADDING_MINUS_RASE;
        int y = e.getY() - PADDING_MINUS_RASE;
        int dx = x % CELL_W;
        int dy = y % CELL_H;

        x /= CELL_W;
        y /= CELL_H;

        aroundPoint = dx <= DIAMETER && dy <= DIAMETER;

        if (lastPointedPointOK && x == lastPointedPoint.x && y == lastPointedPoint.y && aroundPoint)
            return;

        repaintLastPointedPointIntersection(getGraphics());

        lastPointedPoint.x = x;
        lastPointedPoint.y = y;

        paintCurrentPointedPointIntersection(getGraphics());
    }

    private void paintCurrentPointedPointIntersection(Graphics g) {
        if (lastPointedPointOK = (aroundPoint && isNullPoint(lastPointedPoint.x, lastPointedPoint.y))) {
            int x = lastPointedPoint.x * CELL_W + PADDING_MINUS_RASE;
            int y = lastPointedPoint.y * CELL_H + PADDING_MINUS_RASE;

            g.copyArea(x, y, DIAMETER, DIAMETER, 0, PANEL_H - y);
            g.setColor(myTurn ? okColor : nokColor);
            g.fillOval(x, y, DIAMETER, DIAMETER);
        }
    }

    private void repaintLastPointedPointIntersection(Graphics g) {
        if (lastPointedPointOK) {
            int x = lastPointedPoint.x * CELL_W + PADDING_MINUS_RASE;
            int y = lastPointedPoint.y * CELL_H + PADDING_MINUS_RASE;

            g.setColor(backgroundColor);
            g.fillOval(x, y, DIAMETER, DIAMETER);
            g.setColor(lineColor);
            g.drawLine(x + DIAMETER / 2, y, x + DIAMETER / 2, y + DIAMETER);
            g.drawLine(x, y + DIAMETER / 2, x + DIAMETER, y + DIAMETER / 2);

            //int dy = PANEL_H - y;
            //g.copyArea( x, PANEL_H, DIAMETER, DIAMETER, 1, -dy + 1);
            //g.copyArea( x + 10, y + 10, DIAMETER, DIAMETER, -10, -10);
            //update(g);

            aroundPoint = false;
            lastPointedPointOK = false;
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        setStaticRectAlign(event.getActionCommand());
    }

    private void setStaticRectAlign(final String actionCommand) {
        boolean _1 = true, _2 = true, _3 = true, _4 = true;
        switch (actionCommand) {
            case "1":
                STATICRECT_X = 10;
                STATICRECT_Y = 10;
                _1 = false;
                break;
            case "2":
                STATICRECT_X = _WIDTH - 10 - STATICRECT_W;
                STATICRECT_Y = 10;
                _2 = false;
                break;
            case "3":
                STATICRECT_X = 10;
                STATICRECT_Y = _HEIGHT - 10 - STATICRECT_H;
                _3 = false;
                break;
            case "4":
                STATICRECT_X = _WIDTH - 10 - STATICRECT_W;
                STATICRECT_Y = _HEIGHT - 10 - STATICRECT_H;
                _4 = false;
                break;
            default:
                return;
        }
        setTopLeftAlignment.setEnabled(_1);
        setTopRightAlignment.setEnabled(_2);
        setBottomLeftAlignment.setEnabled(_3);
        setBottomRightAlignment.setEnabled(_4);
        relocation(mobilRect.getLocation(), 0, 0);
        repaint();
    }

    private void setMyPoint(final int x, final int y) {
        pointsTable[x][y] |= MY_POINT;
    }

    private void setPartnerPoint(final int x, final int y) {
        pointsTable[x][y] |= PARTNER_POINT;
    }

    private void setMyPrisonierPoint(final int x, final int y) {
        pointsTable[x][y] &= ~PARTNER_PRISONIER_POINT;
        pointsTable[x][y] |= MY_PRISONIER_POINT;
    }

    private void setPartnerPrisonierPoint(final int x, final int y) {
        pointsTable[x][y] &= ~MY_PRISONIER_POINT;
        pointsTable[x][y] |= PARTNER_PRISONIER_POINT;
    }

    private void setPrisonierPoint(final int mask, final int x, final int y) {
        switch (mask) {
            case MY_PRISONIER_POINT:
                setMyPrisonierPoint(x, y);
                break;
            case PARTNER_PRISONIER_POINT:
                setPartnerPrisonierPoint(x, y);
                break;
        }
    }

    private void setMarkPoint(final int x, final int y) {
        pointsTable[x][y] |= MARK_POINT;
    }

    private void setMarkID(final int x, final int y, final int ID) {
        setMarkPoint(x, y);
        pointsTable[x][y] &= ~NULL_POINT;
        pointsTable[x][y] |= ID;
    }

    private void setNullPoint(final int x, final int y) {
        pointsTable[x][y] = NULL_POINT;
    }

    private boolean isMyPoint(final int x, final int y) {
        return 0 != (pointsTable[x][y] & MY_POINT);
    }

    private boolean isPartnerPoint(final int x, final int y) {
        return 0 != (pointsTable[x][y] & PARTNER_POINT);
    }

    private boolean isEmptyPoint(final int x, final int y) {
        return !isPartnerPoint(x, y) && !isMyPoint(x, y);
    }

    private boolean isMyPrisonierPoint(final int x, final int y) {
        return 0 != (pointsTable[x][y] & MY_PRISONIER_POINT);
    }

    private boolean isPartnerPrisonierPoint(final int x, final int y) {
        return 0 != (pointsTable[x][y] & PARTNER_PRISONIER_POINT);
    }

    private boolean isFreePoint(final int x, final int y) {
        return !isPartnerPrisonierPoint(x, y) && !isMyPrisonierPoint(x, y);
    }

    private boolean isMarkPoint(final int x, final int y) {
        return 0 != (pointsTable[x][y] & MARK_POINT);
    }

    private boolean isNullPoint(final int x, final int y) {
        return isFreePoint(x, y) && isEmptyPoint(x, y);
    }

    private void clearMarkPoint(final int x, final int y) {
        pointsTable[x][y] &= ~MARK_POINT;
    }

//    static private final int

    private void clearMarkID(final int x, final int y) {
        clearMarkPoint(x, y);
        pointsTable[x][y] &= ~NULL_POINT;
    }

    private int getMarkID(final int x, final int y) {
        return isMarkPoint(x, y) ? (pointsTable[x][y] & NULL_POINT) : NULL;
    }

    private boolean findNextPoint(final int x, final int y, final int markID, final int POINT_TYPE) {
//        pointsTable[x][y] = pointsTable[x][y];
//        System.out.print("|" + x + "," + y);
        boolean rez = false;
        begin:
        {
            if ((0 != (pointsTable[x][y] & POINT_TYPE)) && isFreePoint(x, y)) {
                if (!isMarkPoint(x, y)) {
                    setMarkID(x, y, markID);
                    /*1*/
                    try {
                        if (rez = findNextPoint(x - 1, y - 1, markID + 1, POINT_TYPE)) break begin;
                    } catch (ArrayIndexOutOfBoundsException ex) {
                    }
                    /*2*/
                    try {
                        if (rez = findNextPoint(x, y - 1, markID + 1, POINT_TYPE)) break begin;
                    } catch (ArrayIndexOutOfBoundsException ex) {
                    }
                    /*3*/
                    try {
                        if (rez = findNextPoint(x + 1, y - 1, markID + 1, POINT_TYPE)) break begin;
                    } catch (ArrayIndexOutOfBoundsException ex) {
                    }
                    /*4*/
                    try {
                        if (rez = findNextPoint(x + 1, y, markID + 1, POINT_TYPE)) break begin;
                    } catch (ArrayIndexOutOfBoundsException ex) {
                    }
                    /*5*/
                    try {
                        if (rez = findNextPoint(x + 1, y + 1, markID + 1, POINT_TYPE)) break begin;
                    } catch (ArrayIndexOutOfBoundsException ex) {
                    }
                    /*6*/
                    try {
                        if (rez = findNextPoint(x, y + 1, markID + 1, POINT_TYPE)) break begin;
                    } catch (ArrayIndexOutOfBoundsException ex) {
                    }
                    /*7*/
                    try {
                        if (rez = findNextPoint(x - 1, y + 1, markID + 1, POINT_TYPE)) break begin;
                    } catch (ArrayIndexOutOfBoundsException ex) {
                    }
                    /*8*/
                    try {
                        if (rez = findNextPoint(x - 1, y, markID + 1, POINT_TYPE)) break begin;
                    } catch (ArrayIndexOutOfBoundsException ex) {
                    }
                } else if (FIRST_MARK_ID == getMarkID(x, y) && markID >= FIRST_MARK_ID + 4) {
                    return true;
                }
            }
        }
        if (rez) {
            pixelsPolygon.addPoint(x * CELL_W + PADDING, y * CELL_H + PADDING);
            pointsPolygon.addPoint(x, y);
        }

        if (markID == getMarkID(x, y)) {
            clearMarkPoint(x, y);
        }

        return rez;
    }

    private void updateStatistic(final int mask) {
        Rectangle bounds = pointsPolygon.getBounds();
        /*
        m - last line
        n - last column
        */
        int m = bounds.height + bounds.y, n = bounds.width + bounds.x, x, y;


        for (int i = bounds.x, j; i <= n; ++i) {
            x = i * CELL_W + PADDING;
            for (j = bounds.y; j <= m; ++j) {
                y = j * CELL_H + PADDING;
//                System.out.println("test for x = " + x + ", y = " + y + " point");
                if (pixelsPolygon.contains(x - 1, y) &&
                        pixelsPolygon.contains(x + 1, y) &&
                        pixelsPolygon.contains(x, y - 1) &&
                        pixelsPolygon.contains(x, y + 1)
                        ) {
//                    System.out.println("       accept for x = " + x + ", y = " + y + ", i = " + i + ", j = " + j + " point");
                    setPrisonierPoint(mask, i, j);
                }
            }
        }
    }
}

class LocalJMenuItem extends JMenuItem {
    public LocalJMenuItem(String text, String actionCommand, ActionListener listener, JPopupMenu popup) {
        super(text);

        setActionCommand(actionCommand);
        addActionListener(listener);
        popup.add(this);
    }
}
