/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static com.me0x.dots.client.Constants.CONNECT_REQUEST_FAIL;
import static com.me0x.dots.client.Constants.CONNECT_REQUEST_OK;
import static com.me0x.dots.client.Constants.CSERVER_IP;
import static com.me0x.dots.client.Constants.CSERVER_PORT;
import static com.me0x.dots.client.Constants.EXIT;
import static com.me0x.dots.client.Constants.FILE;
import static com.me0x.dots.client.Constants.HAVE_PARTNER;
import static com.me0x.dots.client.Constants.I_DISCONNECT;
import static com.me0x.dots.client.Constants.MESSAGE;
import static com.me0x.dots.client.Constants.ME_DISCONNECT;
import static com.me0x.dots.client.Constants.POINT;

/**
 * @author Teodor MAMOLEA
 */
class Client {
    private final TrayIcon trayIcon;
    private final FirstFrame frame1;
    private final SecondFrame frame2;
    private final Image internetOkImage;
    private final Image internetNokImage;
    private Socket socket;
    private PrintWriter out;
    private Scanner in;
    private String ID;
    private String PW;

    //private final Image myPointTImage;
    //private final Image partnerPointTImage;

    Client(final TrayIcon trayIcon) {
        this.trayIcon = trayIcon;

        internetOkImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/image/internetOk.png"));
        internetNokImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/image/internetNok.png"));

//        final Client _this = this;
//        EventQueue.invokeLater( new Runnable() {
//            @Override
//            public void run() {
//                frame1 = new FirstFrame( _this );
//            }
//        });
//        EventQueue.invokeLater( new Runnable() {
//            @Override
//            public void run() {
//                frame2 = new SecondFrame( _this );
//            }
//        });
        frame1 = new FirstFrame(this);
        frame2 = new SecondFrame(this, trayIcon);
    }

    void run() {
        begin:
        while (true) {
            trayIcon.setImage(internetNokImage);
            frame1.setID_PW("-", "-");
            setFramesVisible(true, false);
            /* create socket */
            while (true) {
                try {
                    socket = new Socket(CSERVER_IP, CSERVER_PORT);
                    in = new Scanner(socket.getInputStream());
                    out = new PrintWriter(socket.getOutputStream(), true);

                    final String input = in.nextLine();

                    ID = input.substring(0, 9);
                    PW = input.substring(9);

                    frame1.setID_PW(ID, PW);

                    break; /* create socket */
                } catch (final IOException e) {
                    System.out.println("Exception: " + e);
                    sleep(3000);
                }
            }

            wait_partner:
            while (true) {
                trayIcon.setImage(internetOkImage);
                try {
                    setFramesVisible(true, false);

                    if (!waitPartner())
                        break begin;

                    setFramesVisible(false, true);

//                    System.out.println("ID = " + ID + " is run!");

                    while (true) {
                        final String input = in.nextLine();

                        switch (input.charAt(0)) {
                            case ME_DISCONNECT:
                                send(input);
                            case I_DISCONNECT:
                                frame2.clear();
                                continue wait_partner;
                            case MESSAGE:
                            case POINT:
                            case FILE:
                                frame2.input(input);
                                break;
                            default:
//                                System.out.println("Unknown input!!! '" + input + "'");
                        }
                    }
                } catch (final NoSuchElementException e) {
                    continue begin;
                }
            }
        }

        close();
    }

    private void setFramesVisible(final boolean frame1, final boolean frame2) {
        this.frame1.setVisible(frame1);
        this.frame2.setVisible(frame2);
    }

    private boolean waitPartner() throws NoSuchElementException {
        String input;

        while (true) {
            input = in.nextLine();
//            System.out.println("waitPartner input = " + input);
            switch (input.charAt(0)) {
                case CONNECT_REQUEST_FAIL:
//                    System.out.println("waitPartner CONNECT_FAIL ");
                    frame1.retry("ID and PASSWORD not coincid!");
                    break;
                case HAVE_PARTNER:
//                    System.out.println("waitPartner HAVE_PARTNER ");
                    frame2.setMyTurn(false);
                    //out.println( input );
                    send(input);
                    return true;
                case CONNECT_REQUEST_OK:
                    frame2.setMyTurn(true);
//                    System.out.println("waitPartner CONNECT_OK ");
                    return true;
                case EXIT:
                    return false;
            }
        }
    }

    synchronized void send(final String output) {
        out.println(output);
    }

    private void close() {
        try {
            frame1.setVisible(false);
            frame2.setVisible(false);

            //out.println("" + Constants.EXIT);

            in.close();
            out.close();
            socket.close();
        } catch (final IOException ex) {
        } finally {
            System.exit(0);
        }
    }

    private void sleep(final long millis) {
        try {
            Thread.sleep(millis);
        } catch (final InterruptedException ex) {

        }
    }
}
