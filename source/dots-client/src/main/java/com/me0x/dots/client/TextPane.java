/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * @author Teodor MAMOLEA
 */
public class TextPane extends JPanel {
    private final static String lineSeparator = System.getProperty("line.separator");
    private final JTextPane textPane;
    private final StyledDocument styledDocument;

    public TextPane() {
        setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane(textPane = new JTextPane());

        textPane.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
        textPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        textPane.setCursor(new Cursor(Cursor.TEXT_CURSOR));
        textPane.setDragEnabled(false);
        textPane.setEditable(false);
        for (MouseListener l : textPane.getMouseListeners()) {
            textPane.removeMouseListener(l);
        }
        for (MouseMotionListener l : textPane.getMouseMotionListeners()) {
            textPane.removeMouseMotionListener(l);
        }
        ((DefaultCaret) textPane.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        //scrollPane.setBorder( new LineBorder( Color.GRAY, 1 ));
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        styledDocument = textPane.getStyledDocument();
        initStyles();

        add(scrollPane);
    }

    public void inputMessage(final String content) {
        String name = "unamed";
        String time = "12:13:14";
        try {
//            styledDocument.insertString( styledDocument.getLength(), name + lineSeparator, styledDocument.getStyle( "name"));
//            styledDocument.insertString( styledDocument.getLength(), content + lineSeparator, styledDocument.getStyle( "message"));
//            styledDocument.insertString( styledDocument.getLength(), "time - " + time + lineSeparator, styledDocument.getStyle( "time"));
//            final javax.swing.JTextArea messageArea = new javax.swing.JTextArea( content );
//            messageArea.setPreferredSize( new java.awt.Dimension( 150, 100) );
//            messageArea.setMaximumSize( messageArea.getPreferredSize() );
//            messageArea.setBackground( java.awt.Color.YELLOW);
//            messageArea.setLocation( 10, 0);
//            messageArea.setLineWrap( true );
//            messageArea.setWrapStyleWord( true );

            MessageArea messageArea = new MessageArea(content.substring(1), content.charAt(0));

            textPane.insertComponent(messageArea);
//            messageArea.addMouseListener( new MouseAdapter() {
//                @Override
//                public void mousePressed( MouseEvent event) {
//                    textPane.remove( textPane.getComponent( 0 ));
//                    textPane.repaint();
//                }
//            });yledDocument.getLength(), lineSeparator, null
            styledDocument.insertString(styledDocument.getLength(), lineSeparator, null);
            textPane.revalidate();

        } catch (BadLocationException ex) {
//            System.out.println("TextPane exception");
        }
    }

    public void inputFile(final JPanel panel) {
//        panel.setMaximumSize( new Dimension( 160, 150) );
        textPane.insertComponent(panel);
        try {
            styledDocument.insertString(styledDocument.getLength(), lineSeparator, null);
        } catch (BadLocationException ex) {

        }
    }

    public void clear() {
        textPane.removeAll();
    }

    private void initStyles() {
        javax.swing.text.Style s;

        s = styledDocument.addStyle("name", null);
        StyleConstants.setItalic(s, true);
        StyleConstants.setFontSize(s, 16);
        StyleConstants.setFontFamily(s, Font.SANS_SERIF);
        StyleConstants.setUnderline(s, true);

        s = styledDocument.addStyle("message", null);
        StyleConstants.setFontSize(s, 14);
        StyleConstants.setBold(s, true);
        StyleConstants.setBackground(s, new Color(0, 0, 0, 50));
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_JUSTIFIED);

        s = styledDocument.addStyle("time", null);
        StyleConstants.setFontSize(s, 8);
        StyleConstants.setItalic(s, true);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_RIGHT);

        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(30, 30));
        panel.setSize(panel.getPreferredSize());
        panel.setBackground(Color.YELLOW);
        panel.setAlignmentY(0.8f);
    }
}
