/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.SoftBevelBorder;

import static com.me0x.dots.client.Constants.FILE;
import static com.me0x.dots.client.Constants.MESSAGE;
import static com.me0x.dots.client.Constants.MY_MESSAGE;
import static com.me0x.dots.client.Constants.NULL;
import static com.me0x.dots.client.Constants.PARTNER_MESSAGE;
import static javax.swing.JSplitPane.VERTICAL_SPLIT;
import static javax.swing.border.BevelBorder.LOWERED;

/**
 * @author Teodor MAMOLEA
 */
strictfp class ChatPanel extends JPanel {

    private final SecondFrame parent;
    private final TextPane textPane;
    private final TextArea textArea;
    private String lastMessage;

    ChatPanel(final SecondFrame parent, final int w, final int h) {
        this.parent = parent;

        setPreferredSize(new Dimension(w, h));

        setBorder(new SoftBevelBorder(LOWERED));
        setLayout(new BorderLayout());

        textArea = new TextArea(this);
        textPane = new TextPane();

        final JSplitPane splitPane = new JSplitPane(VERTICAL_SPLIT, textPane, textArea);

        splitPane.setResizeWeight(0.8);
        splitPane.setContinuousLayout(true);

        add(splitPane);
    }

    void input(final String string) {
        switch (string.charAt(0)) {
            case MESSAGE:
                textPane.inputMessage(PARTNER_MESSAGE + string.substring(1));
                break;
            case FILE:
                textPane.inputFile(new FileReceivesPanel(string.substring(1)));
//                new Thread(new FileReceives(string.substring(1), panel)).start();
                break;
        }
    }

    void send(final String string) {
        switch (string.charAt(0)) {
            case MESSAGE:
                textPane.inputMessage(MY_MESSAGE + string.substring(1));
                break;
            case FILE:
                try {
                    final JPanel panel = new JPanel();
                    panel.setMaximumSize(new Dimension(160, 150));
                    textPane.inputFile(panel);
                    new Thread(new FileSend(string.substring(1), panel)).start();
                } catch (final IOException ex) {

                }
                break;
            case NULL:
                break;
            default:
                return;
        }

        parent.send(string);

        if (string.charAt(0) != NULL) {
            textPane.inputMessage(MY_MESSAGE + string);
        }

        parent.send(MESSAGE + string);
    }

    void clear() {
        textPane.clear();
        textArea.clear();
    }
}
