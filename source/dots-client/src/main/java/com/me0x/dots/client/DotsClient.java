/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Teodor MAMOLEA
 */
public class DotsClient {
    public static void main(String[] args) {
//        int c = 1;
//        if(c == 1)
//            System.exit( 0 );

        TrayIcon trayIcon = null;
        if (SystemTray.isSupported()) {
            trayIcon =
                    new TrayIcon(
                            Toolkit.getDefaultToolkit().getImage(new DotsClient().getClass().getResource("/image/internetNok.png"))
                    );
            trayIcon.setImageAutoSize(true);

            SystemTray tray = SystemTray.getSystemTray();
//            PopupMenu popup = new PopupMenu();

//            popup.add( new MenuItem("First") );
//            popup.addSeparator();
//            popup.add( new MenuItem("Second") );
//            popup.add( new MenuItem("Thirs") );
//            popup.add( new MenuItem("About") );
//            popup.addSeparator();
//            popup.add( new MenuItem("Exit") );
//
//            trayIcon.setPopupMenu( popup );

            trayIcon.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
//                    System.out.println("Action on TrayIcon");
                }
            });

            try {
                tray.add(trayIcon);
            } catch (AWTException ex) {

            }
        }

        /*Properties prop = new Properties();
        OutputStream out;

        prop.setProperty("dots.server.port.communication", "1234");
        prop.setProperty("dots.server.port.transfer", "1235");
        prop.setProperty("dots.server.ip.communication", "127.0.0.1");
        prop.setProperty("dots.server.ip.transfer", "127.0.0.1");

        try {
            String pathToProperties = System.getProperty("user.home")
                    + System.getProperty("file.separator")
                    + "Dots"
                    + System.getProperty("file.separator")
                    + "dots.properties";

            File file = new File(pathToProperties);

            if(!file.exists()) {
                out = new FileOutputStream(pathToProperties);
                prop.store(out, null);
                out.flush();
                out.close();
            }
        } catch(Exception ignored) { }
        */


        Map settings = new HashMap<String, String>();
        settings.put("COMMUNICATION SERVER PORT", "" + Constants.CSERVER_PORT);
        settings.put("TRANSFER SERVER PORT", "" + Constants.TSERVER_PORT);
        settings.put("COMMUNICATION SERVER IP", "" + Constants.CSERVER_IP);
        settings.put("TRANSFER SERVER IP", "" + Constants.TSERVER_IP);

        try (
                Scanner in = new Scanner(
                        new FileInputStream(
                                System.getProperty("user.home")
                                        + System.getProperty("file.separator")
                                        + "Dots"
                                        + System.getProperty("file.separator")
                                        + "dots_client_settings"
                        )
                )
        ) {
            while (in.hasNextLine()) {
                String line = in.nextLine().trim();
                if ('#' == line.charAt(0))
                    continue;

                StringTokenizer st = new StringTokenizer(line, ":");

                try {
                    settings.put(st.nextToken().trim(), st.nextToken().trim());
                } catch (Exception e) {
                    System.out.println("StringTokenizer.exception: " + e);
                }
            }
        } catch (Exception e) {
            System.out.println("open \"settings\" file exception: " + e);
        }

        for (Object s : settings.keySet()) {
            System.out.println(s + "-" + settings.get(s));
        }

        try {
            Constants.TSERVER_PORT = Integer.parseInt((String) settings.get("TRANSFER SERVER PORT"));
            Constants.CSERVER_PORT = Integer.parseInt((String) settings.get("COMMUNICATION SERVER PORT"));
            Constants.CSERVER_IP = (String) settings.get("COMMUNICATION SERVER IP");
            Constants.TSERVER_IP = (String) settings.get("TRANSFER SERVER IP");
        } catch (Exception e) {
            System.out.println("COMMUNICATION SERVER PORT or TRANSFER SERVER PORT is not correct: " + e);
            System.exit(1);
        }

        new Client(trayIcon).run();
    }
//    public Dots_Client() {}
}
