/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;

/**
 * @author Teodor MAMOLEA
 */
public class StatisticsPanel extends JPanel {
    public final JLabel label1 = new JLabel("0", JLabel.CENTER);
    public final JLabel label2 = new JLabel("0", JLabel.CENTER);
    public final JLabel label3 = new JLabel("0", JLabel.CENTER);
    public final JLabel label4 = new JLabel("0", JLabel.CENTER);
    public final JLabel label5 = new JLabel("0", JLabel.CENTER);
    public final JLabel label6 = new JLabel("0", JLabel.CENTER);
    public final JLabel label7 = new JLabel("0", JLabel.CENTER);
    public final JLabel label8 = new JLabel("0", JLabel.CENTER);
    public final JLabel label9 = new JLabel("0", JLabel.CENTER);
    public final JLabel label10 = new JLabel("0", JLabel.CENTER);
    private final int WIDTH;
    private final int HEIGHT;
    public JTextArea pointsHistory = new JTextArea(
            "| PARTNER |    I    |"
                    + System.getProperty("line.separator")
                    + "---------------------"
                    + System.getProperty("line.separator")
    );

    public StatisticsPanel(final int w, final int h) {
        WIDTH = w;
        HEIGHT = h;

        setPreferredSize(new java.awt.Dimension(w, h));
        setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
        setLayout(new FlowLayout(FlowLayout.CENTER, 2, 2));

        addPanel("My statistics", 120, label1, label2, label3, label4, label5);
        addPanel("Partner statistics", 120, label6, label7, label8, label9, label10);

        pointsHistory.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        pointsHistory.setBorder(new EmptyBorder(5, 5, 5, 5));
        pointsHistory.setCursor(new Cursor(Cursor.TEXT_CURSOR));
        pointsHistory.setEditable(false);

        JScrollPane scroll = new JScrollPane(pointsHistory);
//        ((DefaultCaret)
//                pointsHistory.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
//
        JPanel textAreaPanel = new JPanel(new GridLayout(1, 1));
        textAreaPanel.setBorder(
                new TitledBorder(
                        new SoftBevelBorder(SoftBevelBorder.RAISED), "Points History (LxC)"
                )
        );
        textAreaPanel.setPreferredSize(new Dimension(WIDTH - 10, WIDTH - 10));
        textAreaPanel.add(scroll);

        add(textAreaPanel);
    }

    private void addPanel(String title, int height, JLabel l1, JLabel l2, JLabel l3, JLabel l4, JLabel l5) {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setPreferredSize(new Dimension(WIDTH - 10, height));
        panel.setBorder(new TitledBorder(new SoftBevelBorder(SoftBevelBorder.RAISED), title));

        JPanel center = new JPanel(new GridLayout(5, 1));
        JPanel east = new JPanel(new GridLayout(5, 1));
        east.setPreferredSize(new Dimension(40, height));

        center.add(new JLabel("TOTAL POINTS", JLabel.RIGHT));
        center.add(new JLabel("PRISONIER POINTS", JLabel.RIGHT));
        center.add(new JLabel("DEATH POINTS", JLabel.RIGHT));
        center.add(new JLabel("FREE POINTS", JLabel.RIGHT));
        center.add(new JLabel("CONQUERED AREA", JLabel.RIGHT));

        east.add(l1);
        east.add(l2);
        east.add(l3);
        east.add(l4);
        east.add(l5);


        panel.add(center);
        panel.add(east, BorderLayout.EAST);

        add(panel);
    }
}
