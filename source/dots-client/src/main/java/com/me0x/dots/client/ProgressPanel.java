/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * @author Teodor MAMOLEA
 */
public class ProgressPanel extends JPanel {
    private double percent;
    private Color fillColor;

    public ProgressPanel() {
        this.percent = 0.0;
        this.fillColor = Color.BLUE;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(fillColor);
        g.fillRect(0, 0, (int) (getWidth() * percent), getHeight());
    }

    public void setPercent(double newPercent) {
        percent = (newPercent > percent && newPercent <= 1.0) ? newPercent : percent;
        repaint();
    }
}
