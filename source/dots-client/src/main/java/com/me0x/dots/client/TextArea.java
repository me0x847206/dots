/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

/**
 * @author Teodor MAMOLEA
 */
public class TextArea extends JPanel implements KeyListener, MouseListener {
    private final ChatPanel parent;
    private JTextArea textArea;

    public TextArea(final ChatPanel parent) {
        this.parent = parent;
        setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane(textArea = new JTextArea());
        textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        textArea.setBorder(new EmptyBorder(5, 5, 5, 5));
        textArea.setCursor(new Cursor(Cursor.TEXT_CURSOR));
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
//        textArea.setDragEnabled( true );
        textArea.addKeyListener(this);
        textArea.addMouseListener(this);

        //scrollPane.setBorder( new LineBorder(Color.GRAY, 1 ));

        add(scrollPane);

        textArea.setDropTarget(new DropTarget() {
            @Override
            public synchronized void drop(DropTargetDropEvent event) {
                try {
                    event.acceptDrop(DnDConstants.ACTION_COPY);
                    java.util.List<File> files
                            = (java.util.List<File>) event.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    for (File file : files) {
                        if (file.isFile() && !file.isDirectory()) {
                            parent.send("" + Constants.FILE + System.currentTimeMillis() + " " + file.getAbsolutePath());
                        }
                    }
                } catch (Exception ex) {

                }
            }
        });
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (java.awt.event.KeyEvent.VK_ENTER == e.getKeyCode()) {
            e.consume();
            String text = textArea.getText().trim();
            if (0 == text.length())
                return;
            parent.send(Constants.MESSAGE + text);
            textArea.setText("");
        }
    }

    public void clear() {
        textArea.setText("");
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        parent.send(Constants.NULL + "");
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
