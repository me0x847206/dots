/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Insets;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

/**
 * @author Teodor MAMOLEA
 */
public class TextField extends JTextField {
    public TextField() {
        this("", 10);
    }

    public TextField(final String string, final int columns) {
        super(string, columns);
        setBorder(new MatteBorder(5, 5, 5, 5, Color.GRAY));
        setMargin(new Insets(20, 20, 20, 20));
        setCursor(new Cursor(Cursor.TEXT_CURSOR));
        setHorizontalAlignment(SwingConstants.CENTER);
        setDragEnabled(true);
    }

    public TextField(final String string, final int columns, final boolean editable) {
        this(string, columns);
        setEditable(editable);
    }
}
