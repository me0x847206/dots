/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Teodor MAMOLEA
 */
public class FileReceivesPanel extends JPanel implements ActionListener {
    private final String ID;
    private final ProgressPanel progress;
    private final JLabel status;
    private final JButton button;

    public FileReceivesPanel(final String ID) {
        this.ID = ID;
        setLayout(new BorderLayout());

        progress = new ProgressPanel();
        status = new JLabel("");
        button = new JButton("Save");

        add(button, BorderLayout.EAST);
        add(progress);
        add(status, BorderLayout.SOUTH);

        try {
            (new Thread(new FileReceives(ID, progress, status))).start();
        } catch (IOException ex) {
//            System.out.println("     Thread exception!!!");
        }

//        button.addActionListener( this );
    }

    @Override
    public void actionPerformed(ActionEvent event) {
//        System.out.println("Save file ID = " + ID);
//        EventQueue.invokeLater( new Runnable() {
//
//            @Override
//            public void run() {
//                try {
//                    ( new Thread( new FileReceives( ID, progress, status) )).start();
//                } catch(IOException ex) {
//                    System.out.println("     Thread exception!!!");
//                }
//            }
//
//        });
    }
}
