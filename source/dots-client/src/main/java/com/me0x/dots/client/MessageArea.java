/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * @author Teodor MAMOLEA
 */
public class MessageArea extends JPanel {
    private final JTextArea textArea;

    public MessageArea(final String string, char from) {
        super();

        setMaximumSize(new Dimension(160, 150));
        setLayout(new BorderLayout());

        textArea = new JTextArea(string);
        textArea.setBackground(Constants.MY_MESSAGE == from ?
                Color.LIGHT_GRAY :
                Color.GREEN);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setEditable(false);

        JButton button = new JButton("Button");

        //JScrollPane scroll = new JScrollPane( textArea );
        //scroll.setMaximumSize( new Dimension( 150, 150) );

        add(textArea);
//        add( button, BorderLayout.SOUTH);

//        System.out.println("messageSize = " + getPreferredSize() );
    }
}
