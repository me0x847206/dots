/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.BorderLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Teodor MAMOLEA
 */
public class FileSend implements Runnable {
    private final Socket socket;
    private final FileInputStream fis;
    private final ObjectOutputStream oos;
    private final ObjectInputStream ois;
    private final JLabel status;
    private final ProgressPanel progress;
    private String fileName;

    public FileSend(final String ID, JPanel panel) throws IOException {
        progress = new ProgressPanel();
        status = new JLabel("");

        panel.setLayout(new BorderLayout());
        panel.add(progress);
        panel.add(status, BorderLayout.SOUTH);

        fileName = ID.substring(ID.indexOf(" ") + 1);

        File file = new File(fileName);

        fileName = file.getName();
        fis = new FileInputStream(file);
        status.setText(fileName);

        socket = new Socket(Constants.TSERVER_IP, Constants.TSERVER_PORT);

        oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject("" + Constants.SOURCE + ID);

        ois = new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public void run() {
        try {
            final byte[] buffer = new byte[Constants.TBUFFER_SIZE];

            final int fileSize = fis.available();
            int bytesRead;
            double totalRead = 0.0;

            oos.writeObject(new String(fileSize + " " + fileName));

            while (0 < (bytesRead = fis.read(buffer))) {
                totalRead += bytesRead;

                oos.writeObject(bytesRead);
                oos.writeObject(buffer);

                oos.flush();

                progress.setPercent(totalRead / fileSize);
            }
        } catch (IOException ex) {
//            System.out.println("exception 3");
        } finally {
            try {
                wait(5000);
            } catch (Exception ignored) {
            }
            try {
                fis.close();
                oos.close();
                ois.close();
                socket.close();
            } catch (Exception e) {

            }
        }
    }
}
