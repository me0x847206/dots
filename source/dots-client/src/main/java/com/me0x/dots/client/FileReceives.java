/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JLabel;

/**
 * @author Teodor MAMOLEA
 */
public class FileReceives implements Runnable {
    private final Socket socket;
    private final ObjectInputStream ois;
    private final FileOutputStream fos;
    private final ObjectOutputStream oos;
    private final JLabel status;
    private final ProgressPanel progress;
    private int fileSize = 12000;
    private String fileName = "defaultFileName";

    public FileReceives(final String ID, ProgressPanel progress, final JLabel status) throws IOException {

        this.progress = progress;
        this.status = status;
        socket = new Socket(Constants.TSERVER_IP, Constants.TSERVER_PORT);

        oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject("" + Constants.DESTINATION + ID);
        ois = new ObjectInputStream(socket.getInputStream());

        try {
            fileName = (String) ois.readObject();
            fileSize = Integer.parseInt(fileName.substring(0, fileName.indexOf(' ')));
            fileName = fileName.substring(fileName.indexOf(' ') + 1);
        } catch (Exception ex) {
//            System.out.println("exception ois.readObject()");
        }

        String pathToFile = System.getProperty("user.home")
                + System.getProperty("file.separator")
                + "Dots"
                + System.getProperty("file.separator")
                + "Downloads";


        File file = new File(pathToFile);
        if (!file.exists())
            file.mkdirs();

        status.setText(fileName);
        fos = new FileOutputStream(pathToFile + System.getProperty("file.separator") + fileName);
    }

    @Override
    public void run() {
        byte[] buffer;
        int bytesRead;
        double totalRead = 0.0;
        try {
            do {
                bytesRead = (Integer) ois.readObject();
                buffer = (byte[]) ois.readObject();

                //if(totalRead < fileSize) {
                fos.write(buffer, 0, bytesRead);
                fos.flush();
                //}

                totalRead += bytesRead;
                progress.setPercent(totalRead / fileSize);
            } while (/*Constants.TBUFFER_SIZE == bytesRead*/totalRead < fileSize);
        } catch (IOException | ClassNotFoundException ex) {

        } finally {
            try {
                fos.close();
                ois.close();
                oos.close();
                socket.close();
            } catch (Exception e) {

            }
        }
    }
}
