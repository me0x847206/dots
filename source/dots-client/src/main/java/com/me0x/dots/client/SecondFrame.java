/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author Teodor MAMOLEA
 */
public class SecondFrame extends JFrame implements WindowListener {
    static private final int WIDTH;
    static private final int HEIGHT;

    static {
        GraphicsDevice screen = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        Dimension screenResolution = screen.getDefaultConfiguration().getBounds().getSize();

        WIDTH = ((int) (screenResolution.width * 0.8f));
        HEIGHT = ((int) (screenResolution.height * 0.8f));
    }

    private final Client parent;
    private final TrayIcon trayIcon;
    private final Image myPointImage;
    private final Image partnerPointImage;
    private final Image myPointTImage;
    private final Image partnerPointTImage;
    private final ChatPanel chatPanel;
    private final PaintPanel paintPanel;
    private final StatisticsPanel statisticPanel;
    private final int CHATPANEL_W = 250;
    private final int STATISTICPANEL_W = 200;
    private final int PAINTPANEL_W;
    private final String lineSeparator = System.getProperty("line.separator");
    private java.util.Scanner in;
    private java.io.PrintWriter out;
    private boolean myPointIsLast = false;
    private boolean haveUnreadMessage = false;

    public SecondFrame(final Client parent, final java.awt.TrayIcon trayIcon) {
        this.parent = parent;
        this.trayIcon = trayIcon;

        myPointImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/image/myPoint.png"));
        myPointTImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/image/myPointT.png"));
        partnerPointImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/image/partnerPoint.png"));
        partnerPointTImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/image/partnerPointT.png"));

        PAINTPANEL_W = WIDTH - CHATPANEL_W - STATISTICPANEL_W;

        setSize(WIDTH, HEIGHT);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        paintPanel = new PaintPanel(this, PAINTPANEL_W, HEIGHT - 20);
        chatPanel = new ChatPanel(this, CHATPANEL_W, HEIGHT);
        statisticPanel = new StatisticsPanel(STATISTICPANEL_W, HEIGHT);

        JPanel centerSubpanel = new JPanel(null);
        centerSubpanel.add(paintPanel);

        add(chatPanel, BorderLayout.EAST);
        add(centerSubpanel);
        add(statisticPanel, BorderLayout.WEST);

        addWindowListener(this);
    }

    public void input(final String string) {
        switch (string.charAt(0)) {
            case Constants.MESSAGE:
            case Constants.FILE:
                haveUnreadMessage = true;
                chatPanel.input(string);
                break;
            case Constants.POINT:
                myPointIsLast = false;
                paintPanel.input(string);
                statisticPanel.label6.setText("" + (Integer.parseInt(statisticPanel.label4.getText()) + 1));

                int spacePos = string.indexOf(' ');
                int x = Integer.parseInt(string.substring(1, spacePos));
                int y = Integer.parseInt(string.substring(spacePos + 1));
                statisticPanel.pointsHistory.append("   " + x + "x" + y + lineSeparator);
                break;
        }
        setTrayImage();
    }

    public void send(final String string) {
        switch (string.charAt(0)) {
            case Constants.POINT:
                myPointIsLast = true;
                parent.send(string);

                statisticPanel.label1.setText("" + (Integer.parseInt(statisticPanel.label1.getText()) + 1));

                int spacePos = string.indexOf(' ');
                int x = Integer.parseInt(string.substring(1, spacePos));
                int y = Integer.parseInt(string.substring(spacePos + 1));

                statisticPanel.pointsHistory.append("              " + x + "x" + y + lineSeparator);
                break;
            case Constants.FILE:
            case Constants.MESSAGE:
                parent.send(string);
            case Constants.NULL:
                haveUnreadMessage = false;
                break;
            default:
                return;
        }
        setTrayImage();
    }

    private void setTrayImage() {
        if (myPointIsLast) {
            if (haveUnreadMessage) {
                trayIcon.setImage(myPointTImage);
            } else {
                trayIcon.setImage(myPointImage);
            }
        } else {
            if (haveUnreadMessage) {
                trayIcon.setImage(partnerPointTImage);
            } else {
                trayIcon.setImage(partnerPointImage);
            }
        }
    }

    public void clear() {
//        statisticPanel.clear();
        paintPanel.clear();
        chatPanel.clear();
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        parent.send("" + Constants.I_DISCONNECT);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    public void setMyTurn(boolean myTurn) {
        paintPanel.myTurn = myTurn;
        myPointIsLast = !myTurn;
        setTrayImage();
    }
}
