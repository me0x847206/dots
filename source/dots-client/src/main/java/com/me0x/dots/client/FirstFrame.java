/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Teodor MAMOLEA
 */
public class FirstFrame extends JFrame implements ActionListener, WindowListener {
    private final Client parent;
    private final TextField idField;
    private final TextField pwField;
    private final TextField partnerIDField;
    private final TextField partnerPWField;
    private final JButton connectButton;
    private final JLabel statusLabel;
    private String ID = "-";
    private String PW = "-";

    public FirstFrame(final Client parent) {
        this.parent = parent;

        idField = new TextField(ID, 10, false);
        pwField = new TextField(PW, 10, false);
        partnerIDField = new TextField("", 10);
        partnerPWField = new TextField("", 10);
        connectButton = new JButton("Connect");
        statusLabel = new JLabel("Status");

        setSize(500, 400);
        setResizable(false);
        setLocationRelativeTo(null);
//        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new FlowLayout());

        connectButton.addActionListener(this);
        connectButton.setPreferredSize(new java.awt.Dimension(150, 40));

        add(new LocalTextField("Your ID", idField));
        add(new LocalTextField("Partner ID", partnerIDField));
        add(new LocalTextField("Your PASSWORD", pwField));
        add(new LocalTextField("Partner PASSWORD", partnerPWField));

        add(connectButton);
//        add( idField );
//        add( pwField );
//        add( partnerIDField );
//        add( partnerPWField );
//        add( connectButton );
//        add( statusLabel );

        addWindowListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String id = partnerIDField.getText().trim().replaceAll(" ", "");
        String pw = partnerPWField.getText().trim().replaceAll(" ", "");

        if (9 != id.length() || 4 != pw.length()) {
            return;
        }

        for (int i = 0; i < 9; ++i) {
            try {
                int n = Integer.parseInt("" + id.charAt(i));
            } catch (NumberFormatException e) {
                return;
            }
        }

        for (int i = 0; i < 4; ++i) {
            try {
                int n = Integer.parseInt("" + pw.charAt(i));
            } catch (NumberFormatException e) {
                return;
            }
        }

        partnerIDField.setEditable(false);
        partnerIDField.setText(id);
        partnerPWField.setEditable(false);
        partnerPWField.setText(pw);

        parent.send(Constants.CONNECT_REQUEST + id + pw);
    }

    public void retry(final String status) {
        partnerIDField.setEditable(true);
        partnerPWField.setEditable(true);
        statusLabel.setText(status);
    }

    public void setID_PW(final String ID, final String PW) {
        this.ID = ID;
        this.PW = PW;

        setTitle("Connect Frame " + ID);

        idField.setText(ID);
        pwField.setText(PW);

//        System.out.println("ID = " + ID + ", PW = " + PW);
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent event) {
        try {
            parent.send("" + Constants.EXIT);
        } catch (NullPointerException e) {
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    private class LocalTextField extends JPanel {
        LocalTextField(String string, TextField tf) {
            super();
            setPreferredSize(new Dimension(220, 80));
            setLayout(new GridLayout(2, 1));
            add(new JLabel(string));
            add(tf);
        }
    }
}
