/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.client;

/**
 * @author Teodor MAMOLEA
 */
public class Constants {
    public static final char CONNECT_REQUEST = 'a';
    public static final char CONNECT_TO_ME = 'b';
    public static final char HAVE_PARTNER = 'c';
    public static final char CONNECT_REQUEST_OK = 'd';
    public static final char CONNECT_REQUEST_FAIL = 'e';
    public static final char EXIT = 'f';
    public static final char I_DISCONNECT = 'g';
    public static final char ME_DISCONNECT = 'h';
    public static final char MESSAGE = 'i';
    public static final char MY_MESSAGE = 'j';
    public static final char PARTNER_MESSAGE = 'k';
    public static final char POINT = 'l';
    public static final char NULL = 'm';
    public static final char OK = 'n';
    public static final char CANCEL = 'o';
    public static final char ERROR = 'p';
    public static final char SOURCE = 'r';
    public static final char DESTINATION = 's';
    public static final char FILE = 't';
    public static final int TBUFFER_SIZE = 5120;
    public static int CSERVER_PORT = 8100;
    public static int TSERVER_PORT = 8101;
    public static String CSERVER_IP = "localhost";
    public static String TSERVER_IP = "localhost";
}
