/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.server;

import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * @author Teodor MAMOLEA
 */
public class TransferServer implements Runnable {
    private final java.net.ServerSocket server;
    private final java.util.SortedSet<TransferClient> clients;

    public TransferServer(int PORT) throws java.io.IOException {
        server = new java.net.ServerSocket(PORT);
        clients = java.util.Collections.synchronizedSortedSet(new java.util.TreeSet<TransferClient>());
    }

    @Override
    public void run() {
//        System.out.println("Start TransferServer");
        while (true) {
            String ID = null;
            try {
                Socket socket = server.accept();
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                ID = (String) ois.readObject();

//                System.out.println("     +" + socket + ", ID = " + ID);
                switch (ID.charAt(0)) {
                    case Constants.DESTINATION:
                        TransferClient client = getClient(ID.substring(1));
                        client.setPartner(socket, ois);
                        (new Thread(client)).start();
                        break;
                    case Constants.SOURCE:
                        clients.add(new TransferClient(this, socket, ois, ID.substring(1)));
                        break;
                }
            } catch (java.io.IOException e) {
//                System.out.println("     java.io.IOException1");
            } catch (Exception e) {
//                System.out.println("     Exception!!!");
            }
        }
    }

    private TransferClient getClient(final String ID) {
        for (TransferClient c : clients) {
            if (ID.equals(c.ID)) {
                return c;
            }
        }
        return null;
    }

    public void closeMe(final TransferClient client) {
        if (null == client)
            return;
        try {
            clients.remove(client);
            client.close();
//            System.out.println("     clients.size = " + clients.size());
        } catch (java.io.IOException ex) {

        } catch (Exception e) {

        }
    }
}
