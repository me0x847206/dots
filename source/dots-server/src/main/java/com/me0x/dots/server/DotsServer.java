/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.server;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Teodor MAMOLEA
 */
public class DotsServer {
    public static void main(String[] args) throws java.io.IOException {

        int c_port = 8100, t_port = 8101;

        Map settings = new HashMap<String, String>();
        settings.put("COMMUNICATION SERVER PORT", "" + c_port);
        settings.put("TRANSFER SERVER PORT", "" + t_port);

        try (
                Scanner in = new Scanner(
                        new FileInputStream(
                                System.getProperty("user.home")
                                        + System.getProperty("file.separator")
                                        + "Dots"
                                        + System.getProperty("file.separator")
                                        + "dots_server_settings"
                        )
                )
        ) {
            while (in.hasNextLine()) {
                String line = in.nextLine().trim();
                if ('#' == line.charAt(0))
                    continue;

                StringTokenizer st = new StringTokenizer(line, ":");

                try {
                    settings.put(st.nextToken().trim(), st.nextToken().trim());
                } catch (Exception e) {
                    System.out.println("StringTokenizer.exception: " + e);
                }
            }
        } catch (Exception e) {
            System.out.println("open \"settings\" file exception: " + e);
        }

        for (Object s : settings.keySet()) {
            System.out.println((String) s + "-" + settings.get(s));
        }

        try {
            t_port = Integer.parseInt((String) settings.get("TRANSFER SERVER PORT"));
            c_port = Integer.parseInt((String) settings.get("COMMUNICATION SERVER PORT"));
        } catch (Exception e) {
            System.out.println("COMMUNICATION SERVER PORT or TRANSFER SERVER PORT is not correct: " + e);
            System.exit(1);
        }

//        int c = 1;
//        if(c == 1)
//            System.exit( 0 );

        (
                new Thread(
                        new CommunicationServer(c_port)
                )
        ).start();
        (
                new Thread(
                        new TransferServer(t_port)
                )
        ).start();
    }
}
