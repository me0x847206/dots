/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @author Teodor MAMOLEA
 */
public class TransferClient implements Runnable, Comparable<TransferClient> {
    public final String ID;
    private final TransferServer parent;
    private final java.net.Socket socket;
    private final java.io.ObjectInputStream ois;
    private final java.io.ObjectOutputStream oos;
    private java.net.Socket partnerSocket;
    private java.io.ObjectInputStream partnerOis = null;
    private java.io.ObjectOutputStream partnerOos = null;

    public TransferClient(final TransferServer parent,
                          final Socket socket,
                          final ObjectInputStream ois,
                          final String ID)
            throws java.io.IOException {
        this.parent = parent;
        this.socket = socket;
        this.ois = ois;
        this.ID = ID;

        oos = new ObjectOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        Object o;
        try {
            String s = (String) ois.readObject();
            partnerOos.writeObject(s);                    // fileName + " " + fileSize
            do {
                o = ois.readObject();
                partnerOos.writeObject(o);

                o = ois.readObject();
                partnerOos.writeObject(o);
            } while (true);
        } catch (java.io.IOException ex) {
//            System.out.println("     java.io.IOException2");
        } catch (ClassNotFoundException ex) {
//            System.out.println("     ClassNotFoundException");
        }

        parent.closeMe(this);
    }

    public void setPartner(final java.net.Socket socket,
                           final java.io.ObjectInputStream ois)
            throws java.io.IOException {
        partnerOis = ois;
        partnerOos = new java.io.ObjectOutputStream(socket.getOutputStream());
        partnerSocket = socket;
    }

    @Override
    public int compareTo(final TransferClient c) {
        return ID.compareTo(c.ID);
    }

    public void close() throws java.io.IOException {
        partnerOis.close();
        partnerOos.close();
        partnerSocket.close();

        ois.close();
        oos.close();
//        System.out.println("     -" + socket + ", ID = " + ID);
        socket.close();
    }
}
