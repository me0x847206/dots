/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.server;

/**
 * @author Teodor MAMOLEA
 */
public class CommunicationServer implements Runnable {
    private final java.net.ServerSocket server;
    private final java.util.SortedSet<CommunicationClient> clients;

    public CommunicationServer(int PORT) throws java.io.IOException {
        server = new java.net.ServerSocket(PORT);
        clients
                = java.util.Collections.synchronizedSortedSet(
                new java.util.TreeSet<CommunicationClient>()
        );
    }

    @Override
    public void run() {
//        System.out.println("Start CommunicationServer");
        while (true) {
            CommunicationClient client = null;
            try {
                java.net.Socket socket = server.accept();

                String ID;
                String PW;

                do {
                    ID = generateID();
                    PW = generatePW();
                } while (null != getClient(ID, PW));

                client = new CommunicationClient(this, socket, ID, PW);
                clients.add(client);

                (new Thread(client)).start();

                System.out.println("+" + socket + ", ID = " + ID + ", PW = " + PW);
            } catch (java.io.IOException e) {
                closeMe(client);
            } catch (Exception e) {
//                System.out.println("Exception!!!");
            }
        }
//        System.out.println("Stop server");
    }

    public CommunicationClient getClient(final String ID, final String PW) {
        for (CommunicationClient c : clients) {
            if (c.isYou(ID, PW)) {
                return c;
            }
        }
        return null;
    }

    public void closeMe(final CommunicationClient client) {
        if (null == client)
            return;
        try {
            client.close();
            clients.remove(client);
//            System.out.println("clients.size = " + clients.size());
        } catch (java.io.IOException ex) {

        } catch (Exception e) {

        } finally {
//            System.gc();
//            Runtime.getRuntime().gc();
        }
    }

    private String generateID() {
        String rez = "";
        for (int i = 0; i < 9; ++i) {
            rez += (int) (Math.random() * 10);
        }

        return rez;
    }

    private String generatePW() {
        String rez = "";

        for (int i = 0; i < 4; ++i) {
            rez += (int) (Math.random() * 10);
        }

        return rez;
    }

    private void close() throws java.io.IOException {
        for (CommunicationClient c : clients) {
            closeMe(c);
        }
        server.close();
    }
}
