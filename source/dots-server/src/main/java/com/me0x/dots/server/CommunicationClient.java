/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.dots.server;

/**
 * @author Teodor MAMOLEA
 */
public class CommunicationClient implements Runnable, Comparable<CommunicationClient> {
    private final CommunicationServer parent;
    private final java.net.Socket socket;
    private final String ID;
    private final String PW;
    private final java.io.PrintWriter out;
    private final java.util.Scanner in;
    private CommunicationClient partner;

    public CommunicationClient(final CommunicationServer parent, final java.net.Socket socket, final String ID, final String PW)
            throws java.io.IOException {
        this.parent = parent;
        this.socket = socket;
        this.ID = ID;
        this.PW = PW;

        out = new java.io.PrintWriter(socket.getOutputStream(), true);
        in = new java.util.Scanner(socket.getInputStream());
    }

    @Override
    public void run() {
        out.println(ID + PW);

        begin:
        while (true) {
            try {
                partner = null;
                waitPartner();

                try {
                    while (true) {
                        String input = in.nextLine();
                        switch (input.charAt(0)) {
                            case Constants.I_DISCONNECT:
                                partner.send("" + Constants.ME_DISCONNECT);
                                send(input);
                            case Constants.ME_DISCONNECT:
                                continue begin;
                            case Constants.MESSAGE:
                            case Constants.POINT:
                            case Constants.FILE:
                                partner.send(input);
                                break;
                            default:
                                System.out.println("Unknown input!!! '" + input + "'");
                        }
                    }
                } catch (java.util.NoSuchElementException e) {
                    break; // break begin;
                }
            } catch (java.util.NoSuchElementException e) {
                break; // break begin;
            }
        }
        parent.closeMe(this);

    }

    private void waitPartner() throws java.util.NoSuchElementException {
        String input;
        while (true) {
            input = in.nextLine();
            switch (input.charAt(0)) {
                case Constants.CONNECT_REQUEST:
                    String id = input.substring(1, 10);
                    String pw = input.substring(10);

                    CommunicationClient client = parent.getClient(id, pw);

                    if (null != client &&
                            null == client.getPartner() &&
                            null == partner &&
                            !ID.equals(id) &&
                            !PW.equals(pw)
                            ) {
                        partner = client;
                        partner.setPartner(this);
                        send("" + Constants.CONNECT_REQUEST_OK);
                        partner.send("" + Constants.HAVE_PARTNER);
                        return;
                    } else {
                        send("" + Constants.CONNECT_REQUEST_FAIL);
                    }
                    break;
                case Constants.HAVE_PARTNER:
                    return;
                case Constants.EXIT:
                    send(input);
                    throw new java.util.NoSuchElementException();
            }
        }
    }

    @Override
    public int compareTo(final CommunicationClient c) {
        return ID.compareTo(c.ID);
    }

    synchronized
    public void send(final String string) {
        out.println(string);
    }

    public boolean isYou(final String ID, final String PW) {
        return this.ID.equals(ID) && this.PW.equals(PW);
    }

    public CommunicationClient getPartner() {
        return partner;
    }

    public void setPartner(final CommunicationClient aPartner) {
        partner = aPartner;
    }

    public void close() throws java.io.IOException {
        in.close();
        out.close();
        socket.close();
        System.out.println("-" + socket + ", ID = " + ID + ", PW = " + PW);
    }

    public void finall() {

    }
}
